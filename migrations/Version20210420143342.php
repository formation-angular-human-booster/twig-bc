<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420143342 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_6AB5B471A625945B ON candidat');
        $this->addSql('DROP INDEX UNIQ_6AB5B4716C6E55B5 ON candidat');
        $this->addSql('ALTER TABLE candidat ADD username VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AB5B471F85E0677 ON candidat (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_6AB5B471F85E0677 ON candidat');
        $this->addSql('ALTER TABLE candidat DROP username');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AB5B471A625945B ON candidat (prenom)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AB5B4716C6E55B5 ON candidat (nom)');
    }
}
