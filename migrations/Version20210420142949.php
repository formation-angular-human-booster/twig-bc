<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420142949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AB5B4716C6E55B5 ON candidat (nom)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6AB5B471A625945B ON candidat (prenom)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_6AB5B4716C6E55B5 ON candidat');
        $this->addSql('DROP INDEX UNIQ_6AB5B471A625945B ON candidat');
    }
}
