<?php

namespace App\Form;

use App\Entity\Candidat;
use App\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rlabel', TextType::class, [
                'label'=> 'Label pas rlabel !!!!',
                    'attr' => [
                        'class' => 'form-control'
                    ]
            ]
            )
            ->add('contenu', TextareaType::class, [
                'attr' => [
                'class' => 'form-control'
            ]])
            ->add('submit', SubmitType::class, [
                'attr'=> ['class'=> 'form-control btn-success mt-5']
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
