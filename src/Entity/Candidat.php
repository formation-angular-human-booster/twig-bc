<?php

namespace App\Entity;

use App\Repository\CandidatRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=CandidatRepository::class)
 * @UniqueEntity(
 *     fields={"username"},
 *     message="Un utilisateur avec ce username existe déjà." )
 */
class Candidat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3)
     * @Assert\NotBlank(message="Veuillez saisir un nom !")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;


    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\LessThan("-18 years", message="Il n'est pas majeur !")
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 20,
     *      notInRangeMessage = "Vous devez au moins avoir {{ min }} et au max {{ max }}",
     * )
     */
    private $note;


    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(?float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function __toString(){
        return $this->nom . ' ' . $this->prenom;
    }

    public function toArray(){
        return [
            'nom'=>$this->nom,
            'prenom'=>$this->prenom
        ];
    }
}
