<?php

namespace App\Controller;

use App\Entity\Candidat;
use App\Entity\Question;
use App\Form\CandidatType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CandidatController extends AbstractController
{
    private $candidatRepository;
    private $em;

    public function __construct(EntityManagerInterface $entityManager){
        $this->em = $entityManager;
        $this->candidatRepository = $entityManager->getRepository(Candidat::class);
    }

    /**
     * @Route("/candidat", name="candidat")
     */
    public function index(): Response
    {
        $candidats = $this->candidatRepository->findAll();

        return $this->render('candidat/index.html.twig', [
            'candidats' => $candidats
        ]);
    }

    /**
     * @Route("/candidat/{id}", name="candidat_detail", requirements={"id"="\d+"})
     */
    public function detail(Candidat $candidat): Response
    {
        return $this->render('candidat/detail.html.twig', [
            'candidat' => $candidat,
        ]);
    }

    /**
     * @Route("candidat/add/", name="candidat_add")
     */
    public function add(Request $request): Response
    {
        $form = $this->createForm(CandidatType::class, new Candidat());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){
           $candidat = $form->getData();

           $this->em->persist($candidat);
           $this->em->flush();

            $this->addFlash('success', 'Votre candidat est ajouté !');

            return $this->redirectToRoute('candidat');

        }

        return $this->render('candidat/form.html.twig', [
            'form' => $form->createView(),
            'formErrors'=> $form->getErrors()
        ]);
    }

    /**
     * @Route("/candidat/edit-form/{id}", name="candidat_edit")
     */
    public function edit(Request $request,  Candidat $candidat): Response
    {
        $form = $this->createForm(CandidatType::class , $candidat);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){
            $candidat = $form->getData();

            // Pas obligé !
            // $this->em->persist($candidat);


            $this->em->flush();

            $this->addFlash('success', 'Le candidat '. $candidat->getUsername()  .' a été mis à jour !');

            return $this->redirectToRoute('candidat');

        }

        return $this->render('candidat/form.html.twig', [
            'form' => $form->createView(),
            'formErrors'=> $form->getErrors()
        ]);
    }

    /**
     * @Route("/candidat/delete/{id}", name="candidat_delete")
     */
    public function delete(Candidat $candidat): Response
    {
        $this->em->remove($candidat);
        $this->em->flush();

        $this->addFlash('success', 'Le candidat '. $candidat->getUsername()  .' a été supprimé !');

        return $this->redirectToRoute('candidat');


    }


}
