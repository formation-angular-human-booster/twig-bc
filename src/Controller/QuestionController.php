<?php

namespace App\Controller;

use App\Entity\Question;
use App\Form\QuestionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface  $em){
        $this->em = $em;
    }

    /**
     * @Route("/question", name="question")
     */
    public function index(): Response
    {

        return $this->render('question/index.html.twig', [
            'controller_name' => 'QuestionController',
        ]);
    }

    /**
     * @Route("/question/add", name="question_add")
     */
    public function add(Request $request): Response
    {
        $form = $this->createForm( QuestionType::class, new Question());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $question = $form->getData();
            $this->em->persist($question);
            $this->em->flush();

            $this->addFlash('success', 'Votre question est ajouté !');

            return $this->redirectToRoute('question');
        }


        return $this->render('question/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/question/edit-form/{$id}", name="question_edit")
     */
    public function edit(Request $request, Question $question): Response
    {
        die(dump("todo later !"));

        /*$form = $this->createForm( QuestionType::class, new Question());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $question = $form->getData();
            $this->em->persist($question);
            $this->em->flush();

            $this->addFlash('success', 'Votre question est ajouté !');

            return $this->redirectToRoute('question');
        }


        return $this->render('question/form.html.twig', [
            'form' => $form->createView(),
        ]);*/
    }


}
