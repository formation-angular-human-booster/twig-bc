<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfocoController extends AbstractController
{
    /**
     * @Route("/infoco", name="infoco")
     */
    public function index(): Response
    {
        return $this->render('infoco/index.html.twig', [
            'controller_name' => 'InfocoController',
        ]);
    }

    /**
     * @Route("/infoco/add", name="infoco_add")
     */
    public function add(): Response
    {
        return $this->render('infoco/form.html.twig', [
            'controller_name' => 'InfocoController',
        ]);
    }

    /**
     * @Route("/infoco/{id}", name="infoco_detail")
     */
    public function detail($id): Response
    {
        return $this->render('infoco/detail.html.twig', [
            'id' => $id
        ]);
    }


}
