<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateCommand extends Command
{
    private $em;
    private $passEncoder;
    protected static $defaultName = 'user:create';
    protected static $defaultDescription = 'Cette commande cré un utilisateur avec des roles';

    public function __construct(EntityManagerInterface  $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct(UserCreateCommand::$defaultName);
        $this->em = $em;
        $this->passEncoder = $passwordEncoder;

    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $username = $io->ask("Comment s'appel cet utilisateur ? ");
        $firstName = $io->ask("Quel est son prénom ?");
        $lastname = $io->ask("Quel est son nom");
        $password = $io->askHidden("Quel est le mot de passe de cet personne");
        $isAdmin = $io->confirm("Utilisateur administrateur ? ", false);


        $user = new User();
        $user->setUsername($username)
            ->setFirstname($firstName)
            ->setLastname($lastname)
            ->setPassword($this->passEncoder->encodePassword($user, $password));

        if($isAdmin) {
            $user->setRoles(['ROLE_ADMIN']);
        }

        $this->em->persist($user);
        $this->em->flush();

        return Command::SUCCESS;
    }
}
