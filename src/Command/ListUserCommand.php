<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class ListUserCommand extends Command
{
    protected static $defaultName = 'app:list-user';
    protected static $defaultDescription = 'Super commande qui affiche tous les utilisateurs ! Et qui permet de faire une recherche ! ';

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct(ListUserCommand::$defaultName);
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('nom', InputArgument::OPTIONAL, 'Nom du candidat')
            ->addArgument('prenom', InputArgument::OPTIONAL, 'Prénom du candidat')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $nom = $input->getArgument("nom");
        $prenom = $input->getArgument("prenom");


        $allUser = [];

        if(!is_null($nom)){

            if(!is_null($prenom)){
                $allUser = $this->userRepository->findBy(['lastname'=> $nom, 'firstname'=> $prenom]);
            } else {
                $allUser = $this->userRepository->findBy(['lastname'=> $nom]);
            }
        } else {
            $allUser = $this->userRepository->findAll();
        }

        foreach ($allUser as $user) {
            $io->success($user->getUsername() . '-'. $user->getFirstname() .' '. $user->getLastname() );
        }

        $io->success('Voila tout vos utilisateurs !');

        $result = $io->ask("Comment tu t'appel ?");

        $io->success("Bonjour". $result);

        return Command::SUCCESS;
    }
}
