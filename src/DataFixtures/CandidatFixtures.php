<?php

namespace App\DataFixtures;

use App\Entity\Candidat;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CandidatFixtures extends Fixture
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create("fr_FR");
    }

    public function load(ObjectManager $manager)
    {

        for($i=0;$i<10; $i++){
            $candidat = new Candidat();
            $candidat->setNom($this->faker->firstName);
            $candidat->setPrenom($this->faker->lastName);
            $candidat->setDateNaissance($this->faker->dateTime);
            $candidat->setNote($this->faker->randomNumber(2, true));
            $candidat->setUsername($this->faker->userName);
            $manager->persist($candidat);
        }




        $manager->flush();
    }
}
